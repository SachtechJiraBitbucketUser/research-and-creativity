package com.example.sachtech.researchandcreativity

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.sachtech.researchandcreativity.PostsAdapter.MyHolder
import com.example.sachtech.researchandcreativity.jsons.aganda.DataItem
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import com.example.sachtech.researchandcreativity.ui.home.PostsItemAdapter
import java.text.SimpleDateFormat

class PostsAdapter(mainActivity: HomeActivity, data: List<DataItem?>?) : RecyclerView.Adapter<MyHolder>() {
    var list:List<DataItem?>?=null
    var context:Context?=null


    internal var dateFormat: SimpleDateFormat?=null
    init {
       context=mainActivity
        list=data
        dateFormat = SimpleDateFormat("d MMM")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {

        val itemView: View
        itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_post_layout, parent, false)

        return MyHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        var name = list?.get(position)?.name
        holder.date?.setText(name!!.pasreDate("dd-MM-yyyy","dd"))
        holder.month?.setText(name!!.pasreDate("dd-MM-yyyy","MMM"))
        var manager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        var adapter= PostsItemAdapter(context,list?.get(position)?.items)
        holder.recyclerView?.layoutManager=manager
        holder.recyclerView?.adapter=adapter

    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    inner class MyHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var recyclerView:RecyclerView?=null
        internal var date:TextView?=null
        internal var month:TextView?=null
        init {
            recyclerView = view.findViewById(R.id.postsChildRecyclerView)
            date=view.findViewById(R.id.textViewAgendaGroupDate)
            month=view.findViewById(R.id.textViewAgendaGroupMonth)
        }
    }

    fun  String.pasreDate(fromFormat:String,toFormat:String):String{
        val fmt = SimpleDateFormat(fromFormat)
        val date = fmt.parse(this)
        val fmtOut = SimpleDateFormat(toFormat)
        return fmtOut.format(date)
    }
}
