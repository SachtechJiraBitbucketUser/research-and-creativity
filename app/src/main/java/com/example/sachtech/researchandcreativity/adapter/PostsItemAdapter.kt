package com.example.sachtech.researchandcreativity.ui.home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.jsons.aganda.ItemsItem


class PostsItemAdapter(context: Context?, items: List<ItemsItem?>?) : RecyclerView.Adapter<PostsItemAdapter.MyHolder>() {
internal  var context:Context?=null
    internal var list:List<ItemsItem?>?=null
    init {
        list=items
        this.context=context
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val itemView: View
        itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_postitems_layout, parent, false)

        return MyHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        if (list?.get(position)?.agendaCategory?.equals("Creativity")!!)
            holder.category?.setBackgroundResource(R.drawable.yellow_background)

        holder.time?.setText("START "+list?.get(position)?.agendaStartTime+" END "+list?.get(position)?.agendaEndTime)
        holder.category?.setText(list?.get(position)?.agendaCategory)
        holder.name?.setText(list?.get(position)?.agendaTitle)

    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    inner class MyHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var time:TextView?=null
        internal var category:TextView?=null
        internal var name:TextView?=null
        init {
           time = view.findViewById(R.id.textViewAgendaTime)
            category=view.findViewById(R.id.buttonAgandaCategory)
            name=view.findViewById(R.id.textViewAgendaName)
        }
    }


}
