package com.example.sachtech.researchandcreativity.basepackage.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.sachtech.researchandcreativity.basepackage.controller.FragmentAddTransition
import com.example.sachtech.researchandcreativity.basepackage.controller.FragmentReplaceTransition
import com.example.sachtech.researchandcreativity.basepackage.controller.Navigator

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

abstract class BaseActivity : AppCompatActivity(), BaseAcitivityListener {
    var pref: ModelPrefrence?=null
    private val container = fragmentContainer()
    lateinit var mNavigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectNavigator()
        pref = ModelPrefrence(this)

    }

    private fun injectNavigator() {
        mNavigator = Navigator(this, container, FragmentAddTransition(), FragmentReplaceTransition())
    }

    /**
     * @return navigator to navigate from one screen to another
     */
    override fun getNavigator(): Navigator {
        return mNavigator
    }
    fun showProgress() {
        ProgressDialogFragment.showProgress(supportFragmentManager)
    }

    fun showProgress(s: String) {
        ProgressDialogFragment.showProgress(supportFragmentManager, s)
    }

    fun hideProgress() {
        ProgressDialogFragment.hideProgress()
    }

    /**
     * @return container on which you want to inflate fragment
     * it will be 0 if no fragment is in activity
     */
    abstract fun fragmentContainer(): Int

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1)
            finish()
        else
            super.onBackPressed()
    }
}
