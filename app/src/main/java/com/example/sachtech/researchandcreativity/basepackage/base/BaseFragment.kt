package com.example.sachtech.researchandcreativity.basepackage.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.util.Patterns
import kotlinx.android.synthetic.main.fragment_signup.*
import java.util.regex.Pattern


/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
abstract class BaseFragment : Fragment() {
    val API_KEY="abcd"
    var pref: ModelPrefrence?=null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is BaseAcitivityListener) {
            baseAcitivityListener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(viewToCreate(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = ModelPrefrence(activity!!)
    }

    @LayoutRes
    protected abstract fun viewToCreate(): Int

    fun showProgress() {
        ProgressDialogFragment.showProgress(activity!!.supportFragmentManager)
    }

    fun showProgress(s: String) {
        ProgressDialogFragment.showProgress(activity!!.supportFragmentManager, s)
    }

    fun hideProgress() {
        ProgressDialogFragment.hideProgress()
    }


    companion object {

     lateinit var baseAcitivityListener: BaseAcitivityListener

    }
    fun checkLoginValidation(username: String, password: String): Boolean {
        if (username.equals("")
                || password.equals("") || password.length === 0)
            activity?.showToast(
                    "All fields are required.")
        else if(!Patterns.EMAIL_ADDRESS.matcher(username).matches())
            activity?.showToast(
                    "Your Email Id is Invalid.")
        else if (password.length< 4)
            activity?.showToast(
                    "Password required atlest 6 digit")
        else
            return true

        return false
    }

 fun checkValidation(username: String, fullName: String, email: String, phoneNumber: String): Boolean {
     val ps = Pattern.compile("(?!^\\d+$)^.+$")
     val ms = ps.matcher(username)
        // Check if all strings are null or not
        if (username.equals("")
                || email.equals("") || email.length === 0
                || fullName.equals("") || fullName.length === 0
                || phoneNumber.equals("") || phoneNumber.length === 0
        )

            activity?.showToast(
                    "All fields are required.")
        else
        {
            var errorString=""
            if(!(Pattern.matches("^[a-zA-Z\\.\\'\\-]{2,50}(?: [a-zA-Z\\.\\'\\-]{2,50})+\$",fullName)))
                errorString = errorString + ("Full Name,")
            else
            {

            }
           if (!ms.matches())
               errorString = errorString + ("UserName,")
            else{}
           if (!isValidMobile(phoneNumber))
               errorString= errorString +("Phone Number,")
                else{}
             if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                 errorString= errorString +("E-mail,")
            else{}
            if (errorString.isEmpty())
                return true
            else{

                errorString = errorString.removeSuffix(",")
                activity?.showToast(errorString.toString()+" is invalid")
            }
        }

        // Else do signup or do your stuff
        // Make sure user should check Terms and Conditions checkbox
        // Check if both password should be equal
        // Check if email id valid or not
    return false
    }

    fun isValidMobile(phone: String): Boolean {
        var check = false
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length < 9 || phone.length > 13) {
                // if(phone.length() != 10) {
                check = false
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(phone).matches()
            }
        } else {
            check = false
        }
        return check
    }

}
