package com.example.sachtech.researchandcreativity.basepackage.base

    import android.content.Context
    import android.content.Intent
    import android.support.v7.app.AppCompatActivity
    import android.util.Log
    import android.widget.Toast
    import kotlin.reflect.KClass

    fun Context.showToast(message: CharSequence, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(this, message, duration).show()
    }
        fun Context.showLog(message: String,title:String){
        Log.e(title,message)
    }
        fun Context.openActivity(activity: KClass<out AppCompatActivity>){
        startActivity(Intent(this,activity.java))
    }

    fun Context.networkAvaileble(): Boolean {
       return if (NetworkUtils.isNetworkAvailable(this)) {
        true
    } else {
        false
    }
}
