package com.example.sachtech.researchandcreativity.basepackage.base

import android.content.Context
import android.content.SharedPreferences

class ModelPrefrence(context: Context) {
    internal var preferences: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var facebookUserId: String?=null

    init {
        preferences = context.applicationContext.getSharedPreferences("MyPref", 0)
        editor = preferences.edit()

    }


    fun setFacebookUserId(token: String) {
        this.facebookUserId = token
        editor.putString(FACEBOOK_USERID, token)
        editor.apply()
        editor.commit()
    }
    fun setLogin(token: Boolean) {
        editor.putBoolean(LOGIN, token)
        editor.apply()
        editor.commit()
    }

    fun setFacebookUserame(username: String) {
        editor.putString(FACEBOOK_USERNAME, username)
        editor.apply()
        editor.commit()
    }
    fun setFacebookEmail(email: String) {
        editor.putString(FACEBOOK_EMAIL, email)
        editor.apply()
        editor.commit()
    }
    fun setFacebookFullName(fullname: String) {
        editor.putString(FACEBOOK_FULLNAME, fullname)
        editor.apply()
        editor.commit()
    }
    fun setFacebookProfilePic(profilePic: String) {
        editor.putString(FACEBOOK_PROFILEPIC, profilePic)
        editor.apply()
        editor.commit()
    }
    fun removeEmail(){
        editor.remove(FACEBOOK_EMAIL);
        editor.commit()
        editor.apply()
    }


    fun getFacebookUserId(): String? {
        return preferences.getString(FACEBOOK_USERID, null)
    }fun getLogin(): Boolean? {
        return preferences.getBoolean(LOGIN,false)
    }
    fun getFacebookUserName(): String? {
        return preferences.getString(FACEBOOK_USERNAME, null)
    }
    fun getFacebookEmail(): String? {
        return preferences.getString(FACEBOOK_EMAIL, null)
    }
    fun getFacebookFullName(): String? {
        return preferences.getString(FACEBOOK_FULLNAME, null)
    }
    fun getFacebookProfilePic(): String? {
        return preferences.getString(FACEBOOK_PROFILEPIC, null)
    }

    companion object {

        var FACEBOOK_USERID = "facebookUserId"
        var FACEBOOK_USERNAME="userName"
        var FACEBOOK_FULLNAME="fullName"
        var FACEBOOK_EMAIL="email"
        var FACEBOOK_PROFILEPIC="profile_pic"
        var LOGIN="login"

        var GOOGLE_USERID = "googleUserId"
        var GOOGLE_USERNAME="googleuserName"
        var GOOGLE_FULLNAME="googlefullName"
        var GOOGLE_EMAIL="googleemail"
        var GOOGLE_PROFILEPIC="googleprofile_pic"

        fun getPrefrence(context: Context): ModelPrefrence {
            return ModelPrefrence(context)
        }
    }
    fun setGoogleUserId(token: String) {
        editor.putString(GOOGLE_USERID, token)
        editor.apply()
        editor.commit()
    }

    fun setGoogleUserame(username: String) {
        editor.putString(GOOGLE_USERNAME, username)
        editor.apply()
        editor.commit()
    }
    fun setGoogleEmail(email: String) {
        editor.putString(GOOGLE_EMAIL, email)
        editor.apply()
        editor.commit()
    }
    fun setGoogleFullName(fullname: String) {
        editor.putString(GOOGLE_FULLNAME, fullname)
        editor.apply()
        editor.commit()
    }
    fun setGoogleProfilePic(profilePic: String) {
        editor.putString(GOOGLE_PROFILEPIC, profilePic)
        editor.apply()
        editor.commit()
    }
    fun getGoogleUserId(): String? {
        return preferences.getString(GOOGLE_USERID, null)
    }
    fun getGoogleUserName(): String? {
        return preferences.getString(GOOGLE_USERNAME, null)
    }
    fun getGoogleEmail(): String? {
        return preferences.getString(GOOGLE_EMAIL, null)
    }
    fun getGoogleFullName(): String? {
        return preferences.getString(GOOGLE_FULLNAME, null)
    }
    fun getGoogleProfilePic(): String? {
        return preferences.getString(GOOGLE_PROFILEPIC, null)
    }

}