package com.example.sachtech.researchandcreativity.basepackage.controller

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction


/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

class FragmentAddTransition : Transition {
    override fun performTransiton(transaction: FragmentTransaction, container: Int, baseFragment: Fragment) {
        transaction.add(container, baseFragment)
    }
}
