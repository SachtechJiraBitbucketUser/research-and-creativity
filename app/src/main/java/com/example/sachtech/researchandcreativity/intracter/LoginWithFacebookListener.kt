package sachtech.com.blablademo.firebase.listeners

import com.facebook.AccessToken

/**
 * Created by SachTech on 25-04-2018.
 */

interface LoginWithFacebookListener{

    fun onLoginFailure(error: String)

    fun onLoginSuccess(token: AccessToken)
}
