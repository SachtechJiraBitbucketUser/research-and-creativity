package com.example.sachtech.researchandcreativity.jsons.aganda

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("items")
	val items: List<ItemsItem?>? = null
)