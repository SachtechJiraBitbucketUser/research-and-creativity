package com.example.sachtech.researchandcreativity.jsons.aganda

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class GetAllAgenda(

	@field:SerializedName("Status")
	val status: Boolean? = null,

	@field:SerializedName("Message")
	val message: String? = null,

	@field:SerializedName("data")
	val data: List<DataItem?>? = null
)