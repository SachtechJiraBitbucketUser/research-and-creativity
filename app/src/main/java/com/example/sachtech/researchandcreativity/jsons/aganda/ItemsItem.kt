package com.example.sachtech.researchandcreativity.jsons.aganda

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ItemsItem(

	@field:SerializedName("agenda_title")
	val agendaTitle: String? = null,

	@field:SerializedName("agenda_category")
	val agendaCategory: String? = null,

	@field:SerializedName("agenda_end_time")
	val agendaEndTime: String? = null,

	@field:SerializedName("agenda_add_on")
	val agendaAddOn: String? = null,

	@field:SerializedName("agenda_id")
	val agendaId: String? = null,

	@field:SerializedName("agenda_start_time")
	val agendaStartTime: String? = null
)