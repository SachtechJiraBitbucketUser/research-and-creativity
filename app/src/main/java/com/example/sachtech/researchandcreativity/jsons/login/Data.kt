package com.example.sachtech.researchandcreativity.jsons.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("user_email")
	val userEmail: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("user_mobile")
	val userMobile: String? = null,

	@field:SerializedName("user_fullname")
	val userFullname: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)