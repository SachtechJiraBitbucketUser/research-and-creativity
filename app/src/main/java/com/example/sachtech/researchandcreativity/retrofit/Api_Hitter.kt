package com.example.sachtech.researchandcreativity.retrofit

import com.example.sachtech.researchandcreativity.jsons.aganda.GetAllAgenda
import com.example.sachtech.researchandcreativity.jsons.login.GetLogin
import com.example.sachtech.researchandcreativity.jsons.register.UserRegister
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


/**
 * Created by Balwinder Badgal on 12/14/2017.
 */
object Api_Hitter {
    var retrofit: Retrofit? = null

    fun ApiInterface(): Api_Hitter.AApiInterface {
        var okHttpClient = OkHttpClient ().newBuilder(). addInterceptor( HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        retrofit = Retrofit.Builder()
                .baseUrl("http://stsmentor.com/events/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        return retrofit!!.create(AApiInterface::class.java)

    }

    interface AApiInterface {

        @POST("Event/usersignup/")
        @FormUrlEncoded
        fun userRegister(
                @Field("API_KEY") API_KEY: String,
                @Field("login_type") login_type: String,
                @Field("username") username: String,
                @Field("user_fullname") user_fullname: String,
                @Field("user_email") device_token: String,
                @Field("user_mobile") user_mobile: String,
                @Field("user_password") User_password: String): Call<UserRegister>

        @POST("Event/usersignup/")
        @FormUrlEncoded
        fun userSocialRegister(
                @Field("API_KEY") API_KEY: String,
                @Field("login_type") login_type: String,
                @Field("username") username: String,
                @Field("user_fullname") user_fullname: String,
                @Field("user_email") device_token: String,
                @Field("user_mobile") user_mobile: String,
                @Field("social_user_id") social_user_id: String): Call<UserRegister>


        @GET("Event/usersignin/?API_KEY=abcd&login_type=simple")
        fun simpleLogin(
                @Query("user_email") user_email: String,
                @Query("user_password") user_password: String): Call<GetLogin>

        @GET("Event/usersignin/?API_KEY=abcd")
        fun socialLogin(
                @Query("login_type")login_type: String,
                @Query("social_user_id")social_user_id:String):Call<GetLogin>

        @GET("agenda/agenda_all?API_KEY=abcd")
        fun getAllAganda():Call<GetAllAgenda>


    }
}
