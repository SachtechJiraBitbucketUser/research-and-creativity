package com.example.sachtech.researchandcreativity.ui.home

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.sachtech.researchandcreativity.PostsAdapter
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseActivity
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import com.example.sachtech.researchandcreativity.jsons.aganda.GetAllAgenda
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Response
import android.widget.Toast
import android.R.menu
import android.view.Menu
import android.view.MenuItem
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.ui.login.LoginActivity
import android.view.MenuInflater




class HomeActivity : BaseActivity() {
    override fun fragmentContainer(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        toobar.setTitle("")
        setSupportActionBar(toobar)
        getAllAgenda()
    }
    fun getAllAgenda() {
        showProgress()
        var userSocialRegister= Api_Hitter.ApiInterface().getAllAganda()

        userSocialRegister.enqueue(object : retrofit2.Callback<GetAllAgenda>{
            override fun onFailure(call: Call<GetAllAgenda>?, t: Throwable?) {
                hideProgress()
                showToast(t?.message!!)
            }

            override fun onResponse(call: Call<GetAllAgenda>?, response: Response<GetAllAgenda>?) {
                hideProgress()
                if (response?.body()?.status==true) {
                    var manager: RecyclerView.LayoutManager = LinearLayoutManager(this@HomeActivity, LinearLayoutManager.VERTICAL, false)
                    var adapter= PostsAdapter(this@HomeActivity,response.body()?.data)
                    postRecyclerView.layoutManager=manager
                    postRecyclerView.adapter=adapter
                }
                else{
                    showToast(response?.body()?.message!!)
                }
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.logOut -> {
                Toast.makeText(applicationContext, "Logout Successfully", Toast.LENGTH_LONG).show()
                pref?.setLogin(false)
                openActivity(LoginActivity::class)
                finish()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
