package com.example.sachtech.researchandcreativity.ui.login

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotpasswordFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_forgot_password
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        forgotBackButton.setOnClickListener {
            activity?.onBackPressed()
        }
        forgotPasswordSendButton.setOnClickListener {
            if(editTextForgotEmail.text.toString().equals(""))
                activity?.showToast(
                        "All fields are required.")
            else if (!Patterns.EMAIL_ADDRESS.matcher(editTextForgotEmail.text.toString()).matches())
                activity?.showToast(
                        "Your Email Id is Invalid.")
            else
                activity?.showToast("Message Sent")
        }
    }
}
