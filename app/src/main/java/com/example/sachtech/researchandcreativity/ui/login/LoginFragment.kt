package com.example.sachtech.researchandcreativity.ui.login

import android.os.Bundle
import android.view.View
import com.example.sachtech.researchandcreativity.R
import kotlinx.android.synthetic.main.fragment_login.*
import android.content.Intent
import android.util.Log
import com.facebook.*
import android.content.ContentValues.TAG
import com.example.sachtech.researchandcreativity.basepackage.base.*
import com.example.sachtech.researchandcreativity.jsons.login.GetLogin
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import sachtech.com.blablademo.firebase.FacebookHelper
import sachtech.com.blablademo.firebase.listeners.LoginWithFacebookListener


class LoginFragment : BaseFragment(), GoogleApiClient.OnConnectionFailedListener ,LoginWithFacebookListener{
    override fun onLoginFailure(error: String) {
        activity?.showToast(error)
    }

    override fun onLoginSuccess(token: AccessToken) {
      getUserInformation(token)
    }

  //  var callbackManager:CallbackManager?=null
    lateinit var facebookHelper: FacebookHelper
            var mGoogleApiClient:GoogleApiClient?=null
            private val RC_SIGN_IN = 75


            override fun viewToCreate(): Int {
                return R.layout.fragment_login
            }

            override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
                super.onViewCreated(view, savedInstanceState)

                //callbackManager = CallbackManager.Factory.create()
                facebookHelper= FacebookHelper(this,this)
                signinButton.setOnClickListener{
                    baseAcitivityListener.getNavigator().replaceFragment(SigninFragment::class.java, true)
                }
                signUpButton.setOnClickListener {
                    baseAcitivityListener.getNavigator().replaceFragment(SignupFragment::class.java, true)
                }
                withFacebookButton.setOnClickListener {
                   facebookHelper.signin()
                }
                withGoogleButton.setOnClickListener {
                    signIn()
                  // baseAcitivityListener.getNavigator().replaceFragment(WithGoogleFragment::class.java, true)
                }

                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build()

                mGoogleApiClient = GoogleApiClient.Builder(activity?.applicationContext!!)
                        .enableAutoManage(activity!!,this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build()

            }

    override fun onDetach() {
        super.onDetach()
        mGoogleApiClient=null
    }

    override fun onPause() {
        super.onPause()
            super.onPause();
            mGoogleApiClient?.stopAutoManage(this!!.activity!!)
        mGoogleApiClient?.disconnect()
    }

    fun getFacebookLogin(){
             /*   val EMAIL = "email"
                withFacebookButton.setReadPermissions(Arrays.asList(EMAIL,"public_profile"))
                withFacebookButton.setFragment(this@LoginFragment)
                // If you are using in a fragment, call loginButton.setFragment(this);

                // Callback registration
                withFacebookButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                                // App code
                        getUserInformation(loginResult.accessToken)
                    }

                    override fun onCancel() {
                        activity?.showToast("cancel")
                    }

                    override fun onError(exception: FacebookException) {
                        activity?.showToast("error "+exception.message)
                    }
                })*/
            }

            override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
                facebookHelper.setupCallback(requestCode, resultCode, data)
                if (requestCode === RC_SIGN_IN) {
                    // The Task returned from this call is always completed, no need to attach
                    // a listener.
                    val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                   handleSignInResult(task)
                }
            }

            override fun onConnectionFailed(p0: ConnectionResult) {

            }

            private fun signIn() {
                showProgress()
                val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
                startActivityForResult(signInIntent, RC_SIGN_IN)
            }


            private fun signOut() {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        object : ResultCallback<Status> {
                            override fun onResult(status: Status) {
                               //code here
                            }
                        })
            }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.
                pref?.setGoogleUserId(account.id!!)
            pref?.setGoogleFullName(account.displayName!!)
            pref?.setGoogleEmail(account.email!!)
            pref?.setGoogleProfilePic(account.photoUrl?.toString()!!)
            pref?.setGoogleUserame(account.displayName!!)
            getFinalGoogleLogin(account.id)

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
        }

    }
            private fun getUserInformation(accessToken: AccessToken) {

                val request = GraphRequest.newMeRequest(accessToken)
                { `jsonobject`, response ->
                    pref?.removeEmail()
                    pref?.setFacebookUserame(jsonobject["first_name"].toString())
                    //val  last_name = jsonobject["last_name"].toString()
                        if(!jsonobject.optString("email").toString().isEmpty()) {
                            pref?.setFacebookEmail(jsonobject["email"].toString())
                        }
                    pref?.setFacebookFullName(jsonobject["name"].toString())
                    pref?.setFacebookProfilePic("https://graph.facebook.com/me/picture?access_token=${accessToken.token}")

                    getFacebookLogin(accessToken)
                }
                val parameters = Bundle()
                parameters.putString("fields", "first_name,last_name,name,email,id")
                request.parameters = parameters
                request.executeAsync()

            }

            fun getFacebookLogin(accessToken: AccessToken) {
                pref?.setFacebookUserId(accessToken.userId)
                if (activity?.networkAvaileble()!!) {
                    showProgress()
                    val loginCall = Api_Hitter.ApiInterface().socialLogin("facebook",accessToken.userId)
                    loginCall.enqueue(object : Callback<GetLogin> {
                        override fun onResponse(call: Call<GetLogin>?, response: Response<GetLogin>) {
                            hideProgress()
                             if (response.body()?.status==true) {
                                 pref?.setLogin(true)
                                 activity?.showToast(response.body()?.message!!)
                                 activity?.openActivity(HomeActivity::class)
                                 activity?.finish()
                             }
                             else{
                            baseAcitivityListener.getNavigator().replaceFragment(WithFacebookFragment::class.java, true)
                              }
                        }

                        override fun onFailure(call: Call<GetLogin>?, t: Throwable?) {
                            hideProgress()
                            activity?.showToast(t?.message!!)

                        }

                    })
                }else
                    activity?.showToast("Check internet Connection")
            }
    fun getFinalGoogleLogin(id: String?) {
        if (activity?.networkAvaileble()!!) {
            val loginCall = Api_Hitter.ApiInterface().socialLogin("google", id!!)
            loginCall.enqueue(object : Callback<GetLogin> {
                override fun onResponse(call: Call<GetLogin>?, response: Response<GetLogin>) {
                    if (response.body()?.status==true) {
                        hideProgress()
                        pref?.setLogin(true)
                        activity?.showToast(response.body()?.message!!)
                        activity?.openActivity(HomeActivity::class)
                        activity?.finish()

                    }
                    else{
                        hideProgress()
                        baseAcitivityListener.getNavigator().replaceFragment(WithGoogleFragment::class.java, true)

                    }
                }

                override fun onFailure(call: Call<GetLogin>?, t: Throwable?) {
                    hideProgress()
                    activity?.showToast(t?.message!!)

                }

            })
        }else
            activity?.showToast("Check internet Connection")
    }
        }


