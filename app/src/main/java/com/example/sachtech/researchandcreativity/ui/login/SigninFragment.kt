package com.example.sachtech.researchandcreativity.ui.login

import android.os.Bundle
import android.view.View
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.basepackage.base.networkAvaileble
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_signin.*
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import com.example.sachtech.researchandcreativity.jsons.login.GetLogin
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SigninFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_signin
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtForgotPassword.setOnClickListener {
            baseAcitivityListener.getNavigator().replaceFragment(ForgotpasswordFragment::class.java, true)
        }
        txtSignup.setOnClickListener {
            baseAcitivityListener.getNavigator().replaceFragment(LoginFragment::class.java, true)
        }
        SubmitSigninButton.setOnClickListener {
            val userName=editTextSignInUsername.text.toString()
            val password=editTextSigninPassword.text.toString()

            if (checkLoginValidation(userName,password)) {
                if (activity?.networkAvaileble()!!) {
                    showProgress()
                    val loginCall = Api_Hitter.ApiInterface().simpleLogin(userName, password)
                    loginCall.enqueue(object : Callback<GetLogin> {
                        override fun onResponse(call: Call<GetLogin>?, response: Response<GetLogin>) {
                            hideProgress()
                            if (response.body()?.status==true) {
                                activity?.showToast(response.body()?.message!!)
                                activity?.openActivity(HomeActivity::class)
                                activity?.finish()
                            }
                            else{
                                activity?.showToast(response.body()?.message!!)
                            }
                        }

                        override fun onFailure(call: Call<GetLogin>?, t: Throwable?) {
                            hideProgress()
                            activity?.showToast(t?.message!!)

                        }

                    })
                }else
                    activity?.showToast("Check internet Connection")
            }


        }
        signinBackButton.setOnClickListener {
            activity?.onBackPressed()
        }
    }

}
