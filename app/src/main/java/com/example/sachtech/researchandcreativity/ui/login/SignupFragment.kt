package com.example.sachtech.researchandcreativity.ui.login

import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.view.View
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.basepackage.base.networkAvaileble
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import com.example.sachtech.researchandcreativity.jsons.register.UserRegister
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_signup.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.graphics.BitmapFactory
import android.provider.MediaStore




class SignupFragment : BaseFragment() {
var RESULT_LOAD_IMAGE=48
    var picturePath:String?=null

    override fun viewToCreate(): Int {
        return R.layout.fragment_signup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signupBackButton.setOnClickListener {
            activity?.onBackPressed()
        }

        signupProfileIimagePicker.setOnClickListener(){
            val i = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(i, RESULT_LOAD_IMAGE)
        }

        signUpRegisterButton.setOnClickListener {

            val userName=editTextSignUpUserName.text.toString()
            val fullName= editTextSignUpFullName.text.toString()
            val email= editTextSignUpEmail.text.toString()
            val phoneNumber=  editTextSignUpPhoneNumber.text.toString()
            val password=editTextSignUpPassword.text.toString()
            val login_type="simple";

            if (checkValidation(userName,fullName,email,phoneNumber)) {
                if (activity?.networkAvaileble()!!) {
                    showProgress()
                    val userRegister = Api_Hitter.ApiInterface().userRegister(API_KEY,login_type, userName, fullName, email, phoneNumber, password)
                    userRegister.enqueue(object : Callback<UserRegister> {
                        override fun onResponse(call: Call<UserRegister>?, response: Response<UserRegister>?) {
                            hideProgress()
                            hideProgress()
                            if (response?.body()?.status==true) {
                                pref?.setLogin(true)
                                activity?.showToast(response.body()?.message!!)
                                activity?.openActivity(HomeActivity::class)
                                activity?.finish()
                            }
                            else{
                                activity?.showToast(response?.body()?.message!!)
                            }

                        }


                        override fun onFailure(call: Call<UserRegister>, t: Throwable) {
                            activity?.showToast(t.message!!)
                            hideProgress()
                        }
                    })
                } else
                    activity?.showToast("Check internet Connection")
            }
        }
    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = context?.getContentResolver()?.query(selectedImage, filePathColumn, null, null, null)
            cursor?.moveToFirst()
            val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
            picturePath = cursor?.getString(columnIndex!!)
            cursor?.close()
            signupProfileIimagePicker.setImageBitmap(BitmapFactory.decodeFile(picturePath))
        }
    }
}
