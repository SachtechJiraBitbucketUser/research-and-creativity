package com.example.sachtech.researchandcreativity.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import com.example.sachtech.researchandcreativity.jsons.register.UserRegister
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import com.facebook.CallbackManager
import kotlinx.android.synthetic.main.fragment_withfacebook.*
import retrofit2.Call
import retrofit2.Response


class WithFacebookFragment : BaseFragment() {
    var callbackManager: CallbackManager?=null
    override fun viewToCreate(): Int {
        return R.layout.fragment_withfacebook
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callbackManager = CallbackManager.Factory.create()
        if (pref?.getFacebookProfilePic()!=null){
            facebookProfileIimagePicker.visibility=View.VISIBLE
            Glide.with(this).load(pref?.getFacebookProfilePic()).into(profile_image)
        }
        editTextFacebookUserName.setText(pref?.getFacebookUserName())
        if (pref?.getFacebookEmail()!=null){editTextFacebookEmail.setText(pref?.getFacebookEmail())}
        editTextFacebookFullName.setText(pref?.getFacebookFullName())

        withFacebookBackButton.setOnClickListener {
            activity?.onBackPressed()
        }

        withFacebookRegisterButton.setOnClickListener {
            val userName= editTextFacebookUserName.text.toString()
            val fullName=  editTextFacebookFullName.text.toString()
            val email=  editTextFacebookEmail.text.toString()
            val phoneNumber=   editTextFacebookPhoneNumber.text.toString()
            val login_type="facebook"

            if (checkValidation(userName,fullName,email,phoneNumber))
                registerWithFacebook(userName,fullName,email,phoneNumber,login_type)
            // activity?.openActivity(HomeActivity::class)
        }

    }
    private fun registerWithFacebook(userName: String, fullName: String, email: String, phoneNumber: String, login_type: String) {
        showProgress()
        var userSocialRegister=Api_Hitter.ApiInterface().userSocialRegister(API_KEY,login_type,userName,fullName,email,phoneNumber, pref?.getFacebookUserId()!!)

        userSocialRegister.enqueue(object : retrofit2.Callback<UserRegister>{
            override fun onFailure(call: Call<UserRegister>?, t: Throwable?) {
                hideProgress()
                activity?.showToast(t?.message!!)
            }

            override fun onResponse(call: Call<UserRegister>?, response: Response<UserRegister>?) {
                hideProgress()
                if (response?.body()?.status==true) {
                    pref?.setLogin(true)
                    activity?.showToast(response.body()?.message!!)
                    activity?.openActivity(HomeActivity::class)
                    activity?.finish()
                }
                else{
                    activity?.showToast(response?.body()?.message!!)
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

}
