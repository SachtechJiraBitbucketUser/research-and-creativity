package com.example.sachtech.researchandcreativity.ui.login

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.basepackage.base.showToast
import com.example.sachtech.researchandcreativity.jsons.register.UserRegister
import com.example.sachtech.researchandcreativity.retrofit.Api_Hitter
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_withgoogle.*
import retrofit2.Call
import retrofit2.Response


class WithGoogleFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_withgoogle
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (pref?.getGoogleProfilePic() != null) {
            Glide.with(this).load(pref?.getGoogleProfilePic()).into(googleProfileImage)
        }
        editTextGoogleUserName.setText(pref?.getGoogleUserName())
        if (pref?.getGoogleEmail() != null) {
            editTextGoogleEmail.setText(pref?.getGoogleEmail())
        }
        editTextGoogleFullName.setText(pref?.getGoogleFullName())

        withGoogleBackButton.setOnClickListener {
            activity?.onBackPressed()
        }
        withGoogleRegisterButton.setOnClickListener {
            val userName = editTextGoogleUserName.text.toString()
            val fullName = editTextGoogleFullName.text.toString()
            val email = editTextGoogleEmail.text.toString()
            val phoneNumber = editTextGooglePhoneNumber.text.toString()
            val login_type = "google"

            if (checkValidation(userName, fullName, email, phoneNumber)) {
                registerWithGoogle(userName, fullName, email, phoneNumber, login_type)
                activity?.openActivity(HomeActivity::class)
                activity?.finish()
            }
        }
    }
       fun registerWithGoogle(userName: String, fullName: String, email: String, phoneNumber: String, login_type: String) {
            showProgress()
            var userSocialRegister= Api_Hitter.ApiInterface().userSocialRegister(API_KEY,login_type,userName,fullName,email,phoneNumber, pref?.getGoogleUserId()!!)

            userSocialRegister.enqueue(object : retrofit2.Callback<UserRegister>{
                override fun onFailure(call: Call<UserRegister>?, t: Throwable?) {
                    hideProgress()
                    activity?.showToast(t?.message!!)
                }

                override fun onResponse(call: Call<UserRegister>?, response: Response<UserRegister>?) {
                    hideProgress()
                    if (response?.body()?.status==true) {
                        pref?.setLogin(true)
                        activity?.showToast(response.body()?.message!!)
                        activity?.openActivity(HomeActivity::class)
                        activity?.finish()
                    }
                    else{
                        activity?.showToast(response?.body()?.message!!)
                    }
                }

            })
        }

}
