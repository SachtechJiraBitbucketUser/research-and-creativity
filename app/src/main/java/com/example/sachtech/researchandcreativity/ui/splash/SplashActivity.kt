package com.example.sachtech.gifproject

import android.os.Bundle
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseActivity
import com.example.sachtech.researchandcreativity.ui.splash.SplashFragment
import android.provider.SyncStateContract.Helpers.update
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.util.Base64
import android.util.Log
import com.example.sachtech.researchandcreativity.basepackage.base.openActivity
import com.example.sachtech.researchandcreativity.ui.home.HomeActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

     /*   val info: PackageInfo
        try {
            info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest
                md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }*/
        if (pref?.getLogin()!!) {
            openActivity(HomeActivity::class)
            finish()
        }
        else
       getNavigator().replaceFragment(SplashFragment::class.java, false)

    }

    override fun fragmentContainer(): Int {
        return R.id.container
    }


}
