package com.example.sachtech.researchandcreativity.ui.splash

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import com.example.sachtech.researchandcreativity.R
import com.example.sachtech.researchandcreativity.basepackage.base.BaseFragment
import com.example.sachtech.researchandcreativity.ui.login.LoginActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

class SplashFragment : BaseFragment() {


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        Handler().postDelayed({
            BaseFragment.baseAcitivityListener.getNavigator().openActivity(LoginActivity::class.java)
            activity!!.finish()

        }, 1500)

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_splash
    }
}
